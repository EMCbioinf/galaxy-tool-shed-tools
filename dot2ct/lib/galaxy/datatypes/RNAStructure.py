import logging
log = logging.getLogger(__name__)

from galaxy import util
import galaxy
import galaxy.model
import galaxy.datatypes
import galaxy.datatypes.data

from galaxy.datatypes.metadata import MetadataElement

from galaxy.datatypes.sequence import Sequence
from galaxy.datatypes.tabular import Tabular
from galaxy.datatypes.xml import GenericXml

from galaxy.datatypes.data import Data


import re

class DotBracket ( Sequence ):
    edam_format = "format_1457"
    file_ext = "dbn"
    
    sequence_regexp = re.compile( "^[ACGTURYKMSWBDHVN]*" )
    structure_regexp = re.compile( "^[\(\)\.]*" )
    
    def set_meta( self, dataset, **kwd ):
        """
        Set the number of sequences and the number of data lines
        in dataset.
        """
        if self.max_optional_metadata_filesize >= 0 and dataset.get_size() > self.max_optional_metadata_filesize:
            dataset.metadata.data_lines = None
            dataset.metadata.sequences = None
            dataset.metadata.seconday_structures = None
            return
        
        data_lines = 0
        sequences = 0
        
        for line in file( dataset.file_name ):
            line = line.strip()
            data_lines += 1
            
            if line and line.startswith( '>' ):
                sequences += 1
        
        dataset.metadata.data_lines = data_lines
        dataset.metadata.sequences = sequences
    
    def sniff(self, filename):
        """
        The format is as follows, although it remains unclear whether
        the Dot-Bracket format may contain multiple sequences per file:
        
        >sequenceName1
        CCCaaaGGG
        (((...)))
        >sequenceName2
        GGGuuuCCC
        (((...)))
        """
        
        i = 0
        pairs = False
        
        with open( filename ) as handle:
            for line in handle:
                line = line.strip()
                
                state = i % 3
                
                if state == 0:#header line
                    if(line[0] != '>'):
                        return False
                elif state == 1:#sequence line
                    if not sequence_regexp.match(line.upper()):
                        return False
                    else:
                        sequence_size = len(line)
                elif state == 2:#dot-bracket structure line
                    if (sequence_size != len(line)) or (not structure_regexp.match(line)):
                        return False
                
                i += 1
        return True

class CT( Tabular ):
    edam_format = "format_3309"
    file_ext = "ct"
    
    header_regexp = re.compile( "^[0-9]+" + "(?:\t|[ ]+)" + "[^ \t]+")
    structure_regexp = re.compile( "^[0-9]+" + "(?:\t|[ ]+)" +  "[ACGTURYKMSWBDHVN]+" + "(?:\t|[ ]+)" + "[^\t]+" + "(?:\t|[ ]+)" + "[^\t]+" + "(?:\t|[ ]+)" + "[^\t]+" + "(?:\t|[ ]+)" + "[^\t]+")
    
    def __init__(self, **kwd):
        Tabular.__init__( self, **kwd )
        
        self.columns = 6
        self.column_names = ['base_index', 'base', 'neighbor_left', 'neighbor_right', 'partner', 'natural_numbering']
        self.column_types = ['int', 'str', 'int', 'int', 'int', 'int']

    def set_meta( self, dataset, **kwd ):
        data_lines = 0
        
        for line in file( dataset.file_name ):
            data_lines += 1
        
        dataset.metadata.data_lines = data_lines
    
    def sniff(self, filename):
        
        filename = filename.file_name
        
        i = 0
        with open( filename ) as handle:
            for line in handle:
                line = line.strip()
                
                if(i == 0):
                    if not self.header_regexp.match(line):
                        return False
                else:
                    if not self.structure_regexp.match(line.upper()):
                        return False
                i += 1
        return True


class RNAML( GenericXml ):
    edam_format = "format_3311"
    file_ext = "rnaml"
