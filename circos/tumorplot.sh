#!/bin/bash

# tumorplot.sh $junctions $mastervar $cnvdetails $cnvsegments $gender $outputplot $legend


if [ $# -ne 8 ]
then
    echo "$0 error: unexpected number of arguments"	
    exit   
fi

junctions=$1
masterVar=$2
cnvdetails=$3
cnvsegments=$4
sampleType=$5 
outputplot=$6
legend=$7  
circospackage=$8

# location of scripts for Circos generation and related helper computes
circosutils="$circospackage/bin"

# location of Complete Genomics Circos plot conf files 
confDir="$circospackage/circosConf"


plotType=tumor
plotLabel="tumorplot"


#remove chrM
sed -i '/chrM/d' $junctions
sed -i '/chrM/d' $masterVar
sed -i '/chrM/d' $cnvdetails
sed -i '/chrM/d' $cnvsegments


#create temporary directory
#base=$RANDOM
#echo "random: ${base}"
#outDir=`pwd`/CircosSomatic-${base}
outDir=`pwd`
#mkdir $outDir

# working CNV file containing LAF estimates
cnvDetailsWithLAF=${outDir}/cnvDetailsNonDiploidWithLAF.tsv
touch "${outDir}/cnvDetailsNonDiploidWithLAF.tsv"


### estimate LAF using a single genome; see caveat regarding single-genome LAF estimates
${circosutils}/addLAFestimates.py \
	-m ${masterVar} \
	-c ${cnvdetails} \
	-o ${cnvDetailsWithLAF}


### construct Circos inputs, copy required conf files, run Circos,
### and create html wrapper around resulting png files
${circosutils}/circosPlot.py \
	--junction-file ${junctions} \
	--masterVar-file ${masterVar} \
	--circos-output-dir ${outDir} \
	--plot-label "${plotLabel}" \
	--sample-type ${sampleType} \
	--cnv-details ${cnvDetailsWithLAF} \
	--cnv-segments ${cnvsegments} \
	--conf-dir ${confDir} \
	--plot-type ${plotType}

# rename output files
mv "${outDir}/circos-${plotLabel}.png" ${outputplot}
mv "${outDir}/circosLegend.png" ${legend}



