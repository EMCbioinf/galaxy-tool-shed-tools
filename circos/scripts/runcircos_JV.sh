#!/bin/bash

# usage runcircos_JSV.sh <conffile> <junctions file> <probes file> <snps file> <WG outfile> <montage outfile> <chromosome list> <build> <variations file> <variations file chr> <per-chr file> <custom_outfile> <custum_region>



if [ $# -ne 12 ]
then
	echo "error, unexpected number of arguments in $0"
	echo "$@"
	exit
fi

picsdir="images"
mkdir images
conffile=$1
junctions_file=$2
probes_file=$3
snps_file=$4
wg_out=$5
montage_out=$6
per_chr_file=$7
build=$8
variations=$9
variations_chr=${10}
custom_outfile=${11}
custom_region=${12}

echo ""
echo "runcircos_JV.sh: "
echo "conffile: $conffile, junctionsfile: $junctions_file, probes_file= $probes_file, snps_file= $snps_file wg_out: $wg_out, montage_out: $montage_out "
echo ""

fname=`basename ${junctions_file}`


echo "file = ${junctions_file}" > junctionsfile.txt
echo "file = ${probes_file}" > probesfile.txt
echo "file = ${snps_file}" > snpsfile.txt
echo "file = ${variations}" > snpdensity.txt


cat junctionsfile.txt
cat probesfile.txt
cat snpsfile.txt
cat snpdensity.txt

if [[ $build != "hg18" && $build != "hg19" ]]
then
	echo "error, unknown build"
	exit
fi

if [ $build=="hg18" ]
then
	echo "karyotype = data/karyotype/karyotype.human.hg18.txt" > karyotype.txt
fi

if [ $build=="hg19" ]
then
	echo "karyotype = data/karyotype/karyotype.human.hg19.txt" > karyotype.txt
fi

echo "chromosomes_units = 1000000" > units.txt





#####    Whole Genome plot
if [ ${wg_out} != "None" ] 
then
	echo "chromosomes_display_default = yes" > chromosomes.txt
	echo "color = black" > colorintra.txt
	echo "thickness = 1" >> colorintra.txt

	echo "color = red" > colorinter.txt
	echo "thickness = 4" >> colorinter.txt

	echo "label_size = 30" > labelsize.txt
	echo "show = yes" > showinter.txt
	echo "show = yes" > showintra.txt
	echo "bezier_radius = 0r" >> showinter.txt	

	echo "glyph_size = 2" > glyph.txt
	echo "thickness = 1" > varthickness.txt
	echo "thickness = 4" > junctions_thickness_inter.txt
	echo "thickness = 1" > junctions_thickness_intra.txt
	cat snp_maxval_all.txt > snp_maxval.txt

	#run circos		
	echo ""
	echo "generating whole-genome plot"	
	echo ""
	circos -png -conf ${conffile} 

	#rename and move files
	echo "copying WG plot to galaxy output: mv circosJSV.png ${wg_out}"
	mv circosJV.png ${wg_out}
fi	



#####   Per-chromosome images	
if [[ ${montage_out} != "None" || ${per_chr_file} != "None" ]]
then


	echo "color = black" > colorintra.txt
	echo "thickness = 4" >> colorintra.txt
	echo "label_size = 75" > labelsize.txt
	echo "glyph_size = 4" > glyph.txt
    echo "thickness = 3" > varthickness.txt
	echo "thickness = 6" > junctions_thickness_inter.txt
	echo "thickness = 3" > junctions_thickness_intra.txt
	echo "file = ${variations_chr}" > snpdensity.txt
	
	echo ""
	echo "generating per-chromosome plots"	
	echo ""

	for c in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y
	do
			echo "chromosomes_display_default = no" > chromosomes.txt
			echo "chromosomes = hs$c" >> chromosomes.txt
			echo "show = yes" > showintra.txt
			echo "bezier_radius = 0r" >> showintra.txt
			cat snp_maxval_chr$c.txt > snp_maxval.txt

			# run circos			
			circos -png -conf ${conffile}

			if [[ $c == "X" || $c == "Y" || $c -ge 10 ]]
			then  
				# rename and move files
				mv circosJV.png ${picsdir}/chr$c.png
			else 
				# rename and move files
				mv circosJV.png ${picsdir}/chr0$c.png
				
			fi


	done

	#combine per-chromosome images into 1, remove per-chromosome images
	if [ ${montage_out} != "None" ]
	then	
		echo "combining chromosome plots into montage"
		montage -quality 100 -density 3000  -geometry -10-10 ${picsdir}/chr* "${montage_out}.png"
		mv "${montage_out}.png" ${montage_out}

	fi
	
	#zip per-chromosome plots into single archive
	if [ ${per_chr_file} != "None" ]
	then	
		tar -czf ${per_chr_file} ${picsdir}/chr*
	fi

fi

#####  Custom region
if [ $custom_outfile != "None" ]
then
	echo "Making Custom region plot"
	echo "chromosomes_units = 1" > units.txt
	echo "chromosomes_display_default = no" > chromosomes.txt
	echo "chromosomes = ${custom_region}" >> chromosomes.txt	

	echo "color = black" > colorintra.txt
	echo "thickness = 1" >> colorintra.txt

	echo "color = red" > colorinter.txt
	echo "thickness = 4" >> colorinter.txt

	echo "label_size = 30" > labelsize.txt
	echo "show = yes" > showinter.txt
	echo "show = yes" > showintra.txt
	echo "bezier_radius = 0r" >> showinter.txt	

	echo "glyph_size = 2" > glyph.txt
	echo "thickness = 1" > varthickness.txt
	echo "thickness = 4" > junctions_thickness_inter.txt
	echo "thickness = 1" > junctions_thickness_intra.txt
	cat snp_maxval_all.txt > snp_maxval.txt
	echo "file = ${variations}" > snpdensity.txt

	# run circos			
	circos -png -conf ${conffile}

	# move output
	mv circosJV.png ${custom_outfile}
fi

echo "finished"
		


