#!/bin/bash

#######################################################################################
######### The following commands (sh or bash shell script) illustrate how to  #########
######### generate a 'normal' Circos plot similar to those created by the     #########
######### Complete Genomics pipeline (releases 2.0 and 2.1 currently).        #########
#######################################################################################
    ########################
    ### Prerequisites    ###
    ### 1) python        ###
    ### 2) circos        ###
    ########################
    ###################################################################################
    ### What needs to be modified:                                                  ###
    ### Lines immediately below the comments "RUN-SPECIFIC" or "USER-SITE-SPECIFIC" ###
    ### need to be modified.                                                        ###
    ### - Lines below "RUN-SPECIFIC" must be adjusted appropriately for each run.   ###
    ### - Lines below "USER-SITE-SPECIFIC" must be adjusted to reflect the user's   ###
    ###   site (paths to binaries etc) but can be left alone from run to run.       ###
    ###################################################################################


### type of Circos plot
plotType=normal

### location of normal genome files
##  Note that for normal genome plot creation, 'root' is just a 
##  convenience--all required inputs can be specified individually
##  RUN-SPECIFIC
root=/NA19240/ASM/NA19240-L2-200-37-ASM/EXP/package/GS00706-DNA_C01

### location of crr file (must be correct build [36 or 37]; male can be used regardless of sample sex)
##  RUN-SPECIFIC
refcrr=/home/user/ref/build37.crr


### output destination; create directory if not already present
##  RUN-SPECIFIC
outDir=/home/user/CircosOutput/Normal_NA19240
if test ! -e $outDir; then mkdir $outDir; fi

### masterVar file
masterVarFile=`ls ${root}/ASM/masterVar*tsv.bz2`

### original cnvDetailsNondiploid file
### Used in computing LAF estimates--see caveat regarding single-genome LAF estimates.
### N.B. The rows are used to determine the intervals over which LAF is estimated, but the CNV information
### from this file is not used further, so we can use of the 'Nondiploid' file even for a 'normal' analysis.
origCnvDetailsFile=`ls ${root}/ASM/CNV/cnvDetailsNondiploidBeta*tsv.bz2`

### working CNV file containing LAF estimates
cnvDetailsWithLAF=${outDir}/cnvDetailsNonDiploidWithLAF.tsv

### CNV segments file
cnvSegmentsFile=`ls ${root}/ASM/CNV/cnvSegmentsDiploidBeta*tsv`

### high confidence junctions file
highConfJcns=`ls ${root}/ASM/SV/highConfidenceJunctionsBeta*tsv`

### sample type, based on sex and reference build
##  RUN-SPECIFIC
sampleType=female37

### label to be used in html page wrapping Circos plots
##  RUN-SPECIFIC
plotLabel=normal-NA19240

### location of scripts for Circos generation and related helper computes
##  USER-SITE-SPECIFIC
circosutils=/home/user/circosPackage/bin

### location of Complete Genomics Circos plot conf files
##  USER-SITE-SPECIFIC
confDir=/home/user/circosPackage/circosConf

### path to Circos installation
##  USER-SITE-SPECIFIC
circosbin=/home/user/bin/circos-0.52/bin
export PATH=${circosbin}:${PATH}


### estimate LAF using a single genome; see caveat regarding single-genome LAF estimates
${circosutils}/addLAFestimates.py \
	-m ${masterVarFile} \
	-c $origCnvDetailsFile \
	-o $cnvDetailsWithLAF


### construct Circos inputs, copy required conf files, run Circos,
### and create html wrapper around resulting png files
${circosutils}/circosPlot.py \
	--junction-file ${highConfJcns} \
	--masterVar-file ${masterVarFile} \
	--circos-output-dir ${outDir} \
	--plot-label ${plotLabel} \
	--sample-type ${sampleType} \
	--cnv-details ${cnvDetailsWithLAF} \
	--cnv-segments ${cnvSegmentsFile} \
	--conf-dir ${confDir} \
	--plot-type ${plotType}
