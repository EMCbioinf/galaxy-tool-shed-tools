Installs VirtualNormal Correction Tool

After installing this tool via admin panel, manually configure the following:

1) edit virtual_normal_correction.loc file

   - change "/path/to/hg18.crr" to the location of the Complete Genomics reference crr file on your system
     (can be downloaded from ftp://ftp.completegenomics.com/ReferenceFiles/ )
     
   - change "/path/to/VN_genomes_varfiles_lists_hg18" to the location of the directory containing files with the locations of all the Complete Genomics
     varfiles to be used as a virtual normal. This file should contain 1 file location per line, e.g.
     
            /path/to/normal-varfile-1
            /path/to/normal-varfile-2
            /path/to/normal-varfile-3
            /path/to/normal-varfile-4
            /path/to/normal-varfile-5
            /path/to/normal-varfile-6
            /path/to/normal-varfile-7
            /path/to/normal-varfile-8
   			...
   			
   - edit the tool xml file to offer one or more different sets of virtual normals: 

	[..]
		<!-- edit these options to reflect sets of normal you have available. The values must name files within the directories specified in data_table_conf.xml file -->
		<param name="VNset" type="select" label="Select Virtual Normal set to use" help="1000Genomes set can only be used for hg19 samples, for hg18 54 genomes will be used.">
			<option value="46_diversity.txt" > CG Diversity Panel and trios (54 Genomes) </option>
			<option value="433_1000g.txt" > CG 1000G project genomes (433 Genomes) (hg19 only) </option>
			<option value="479_diversity_1000g.txt" > Diversity and 1000G (479 genomes) (hg19 only) </option>
                        <option value="10_tutorial.txt" > Small VN for tutorial (10 Genomes) </option>
		</param>
	[..]

	the values indicate files expected to be at the location configured in the loc file, 


     So if your .loc file looks like this:


		#loc file for virtual normal tool
		# <columns>value, dbkey, name, VN_genomes_varfiles_lists_location, VN_genomes_junctionfile_lists_location, reference_crr_cgatools</columns>

		hg18	hg18	Virtual Normal hg18	/path/to/VN_genomes_varfiles_lists_hg18/	/path/to/VN_genomes_junctionfiles_lists_hg18/	/path/to/hg18.crr
		hg19	hg19	Virtual_Normal hg19	/path/to/VN_genomes_varfiles_lists_hg19/	/path/to/VN_genomes_junctionfiles_lists_hg19/	/path/to/hg19.crr

     And your xml file like the example above, then the tool expects the following files to exist:
		 /path/to/VN_genomes/varfiles_list_hg18/46_diversity.txt 	
     		 /path/to/VN_genomes/varfiles_list_hg18/433_1000g.txt
		       ...etc..

     and each of these files must contain a 1-per-line list of locations of the varfiles of the normal genomes (or junction files in case of SV analysis).

     Varfiles can be in compressed or uncompressed form. For example, Complete Genomics' Diversity panel can be used.
     (can be downloaded from ftp://ftp2.completegenomics.com/)		


     
 2) restart Galaxy for changes to take effect (alternatively, in the admin panel these setting can be reloaded without the need to restart your galaxy instance)
 
 After this initial setup, additional normals can be added to the lists without having to restart Galaxy. 
