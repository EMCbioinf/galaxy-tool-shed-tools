function [chr, arm, pos, k, d, dd] = loaddata(options)

disp(['Reading data file: ' options.infile]);
[pathstr, name, ext] = fileparts(options.infile);

tmpfile = options.infile;
if strfind(ext, 'gz')
	disp(['Found gzipped file: ' options.infile]);
	disp(['Gunzipping to: ' options.outdir]);
	gunzip(options.infile, options.outdir);		
	tmpfile = fullfile(options.outdir, name);
end
if strfind(ext, 'zip')
	disp(['Found zipped file: ' options.infile]);
	disp(['Unzipping to: ' options.outdir]);
	unzip(options.infile, options.outdir);		
	tmpfile = fullfile(options.outdir, name );
end
[chr, pos, ref, a, c, g, t] = textread(tmpfile, '%n %n %n %n %n %n %n', 'delimiter', ',', 'headerlines', 1);

if strfind(ext, 'gz')
	disp(['Removing temporary file: ' tmpfile]);
	delete(tmpfile);
end
if strfind(ext, 'zip')
	disp(['Removing temporary file: ' tmpfile]);
	delete(tmpfile);
end

X = [ a c g t ];
k = max(X, [], 2);

d = ref + k;
k = min(k, d-k);

N = length(d);
loc = find( rand(1, N) < 0.5 );
k(loc) = d(loc)-k(loc);
k = min(k, d-k);

chrloc = [];
for chrNo = options.chrRange
	loc = find( chr == chrNo );
	chrloc = [ chrloc; loc ];
end

chr = chr(chrloc);
pos = pos(chrloc);
d = d(chrloc);
k = k(chrloc);
N = length(d);
arm = zeros(N, 1);

% assign arm numbers (1 -p, 2 - q)
disp(['Reading hg tables file: ' options.hgtables]);
[ chrArm, chrStart, chrEnd, armNo ] = textread(options.hgtables, '%n %n %n %n', 'headerlines', 1);
for ci = 1 : length(chrArm)

	loc = find( chr == chrArm(ci) & pos >= chrStart(ci) & pos < chrEnd(ci) );
	
	if ~isempty(loc)
		arm(loc) = armNo(ci);
	end

end

%
% GC and MAPPABILITY CORRECTION
%

if ~isempty(options.gcdir) & ~isempty(options.mapdir)

	fprintf('Doing Local GC content and Mappability Correction: ');
	for chrNo = options.chrRange

		fprintf('%g ', chrNo);

		chrloc = find( chr == chrNo );
		if isempty(chrloc)
			continue;
		end

		d_chr = d(chrloc);		
		k_chr = k(chrloc);		
		pos_chr = pos(chrloc);

		% load gc content data
		if ~isempty(options.gcdir)

			if chrNo == 23
				gcfile = [ options.gcdir '/X_1k.txt' ];
			end
			if chrNo == 24
				gcfile = [ options.gcdir '/Y_1k.txt' ];
			end
			if chrNo < 23
				gcfile = [ options.gcdir '/' num2str(chrNo) '_1k.txt' ];
			end
			[gc_start, gc_end, gc_content] = textread(gcfile, '%n %n %n', 'headerlines', 0);
			gc_pos = 0.5*(gc_start + gc_end);

			% sort by genomic position
			[gc_pos, I] = sort(gc_pos);
			gc_content = gc_content(I);
			gc_content = (gc_content - nanmean(gc_content))./nanstd(gc_content);

			% interpolate	
			gc_chr = interp1(gc_pos, gc_content, pos_chr, 'nearest', 'extrap')';
			gc_chr = reshape(gc_chr, size(d_chr));   

		else

			gc_chr = ones(size(d_chr));

		end 

		% load mappability data
		if ~isempty(options.mapdir)

			if chrNo == 23
				mapfile = [ options.mapdir '/X.txt' ];
			end
			if chrNo == 24
				mapfile = [ options.mapdir '/Y.txt' ];
			end
			if chrNo < 23
				mapfile = [ options.mapdir '/' num2str(chrNo) '.txt' ];
			end

			[map_chr, map_pos, map] = textread( mapfile,  '%n %n %n', 'headerlines', 1);

			% sort by genomic position
			[map_pos, I] = sort(map_pos);
			map_content = map_content(I);
			map_content = (map_content - nanmean(map_content))./nanstd(map_content);

			% interpolate		
			map_chr = interp1(map_pos, map_content, pos_chr, 'nearest', 'extrap')';
			map_chr = reshape(map_chr, size(d_chr));  

		else
		
			map_chr = ones(size(d_chr));

		end

		if ~isempty(options.gcdir) & ~isempty(options.mapdir)
			betas = robustfit([gc_chr map_chr], d_chr);
			d_chr = d_chr - betas(2).*gc_chr - betas(3).*map_chr;
			k_chr = k_chr - (k_chr./d_chr).*betas(2).*gc_chr - (k_chr./d_chr).*betas(3).*map_chr;
		end
		if ~isempty(options.gcdir) & isempty(options.mapdir)
			betas = robustfit(gc_chr, d_chr);
			d_chr = d_chr - betas(2).*gc_chr;
			k_chr = k_chr - (k_chr./d_chr).*betas(2).*gc_chr;
		end
		if isempty(options.gcdir) & ~isempty(options.mapdir)
			betas = robustfit([map_chr], d_chr);
			d_chr = d_chr - betas(2).*map_chr;
			k_chr = k_chr - (k_chr./d_chr).*betas(2).*map_chr;
		end

		d(chrloc) = d_chr;
		k(chrloc) = k_chr;

	end
	fprintf('\n');

end

k = reshape(k, [1 N]);
d = reshape(d, [1 N]);
dd = d; 
