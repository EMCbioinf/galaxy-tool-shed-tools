#!/bin/bash

###########################################################################
### The following commands (sh or bash shell script) illustrate how to  ###
### generate a 'tumor' Circos plot similar to those created by the      ###
### Complete Genomics pipeline (releases 2.0 and 2.1 currently).        ###
###########################################################################
    ########################
    ### Prerequisites    ###
    ### 1) python        ###
    ### 2) circos        ###
    ########################
    ###################################################################################
    ### What needs to be modified:                                                  ###
    ### Lines immediately below the comments "RUN-SPECIFIC" or "USER-SITE-SPECIFIC" ###
    ### need to be modified.                                                        ###
    ### - Lines below "RUN-SPECIFIC" must be adjusted appropriately for each run.   ###
    ### - Lines below "USER-SITE-SPECIFIC" must be adjusted to reflect the user's   ###
    ###   site (paths to binaries etc) but can be left alone from run to run.       ###
    ###################################################################################


### type of Circos plot
plotType=tumor

### location of tumor genome files
##  Note that this 'root' specification is just a 
##  convenience used in this example, with the assumption
##  that it points to an intact Complete Genomics export
##  package.  It is possible to specify the locations of
##  all files individually if the file layout has been
##  modified.
##  RUN-SPECIFIC
#root=/HCC2218/ASM/HCC2218-H-200-37-ASM/B/EXP/package/GS00258-DNA_B03
root=/mnt/compgen/Public_Compgen/Cancer_pairs/ASM_Build36_2.0.2/HCC1187/GS00258-DNA_F01

### location of crr file (must be correct build [36 or 37]; male can be used regardless of sample sex)
##  RUN-SPECIFIC
#refcrr=/home/user/ref/build37.crr
refcrr=/mnt/compgen/reference/hg18.crr

### output destination; create directory if not already present
##  RUN-SPECIFIC
outDir=/data/saskia/CircosOutput/Tumor_HCC1187
if test ! -e $outDir; then mkdir $outDir; fi

### masterVar file
masterVarFile=`ls ${root}/ASM/masterVar*tsv.bz2`

### original cnvDetailsNondiploid file
### Used in computing LAF estimates--see caveat regarding single-genome LAF estimates.
origCnvDetailsFile=`ls ${root}/ASM/CNV/cnvDetailsNondiploidBeta*tsv.bz2`

### working CNV file containing LAF estimates
cnvDetailsWithLAF=${outDir}/cnvDetailsNonDiploidWithLAF.tsv

### CNV segments file
cnvSegmentsFile=`ls ${root}/ASM/CNV/cnvSegmentsNondiploidBeta*tsv`

### high confidence junctions file
highConfJcns=`ls ${root}/ASM/SV/highConfidenceJunctionsBeta*tsv`

### sample type, based on sex and reference build
##  RUN-SPECIFIC
sampleType=female37

### label to be used in html page wrapping Circos plots
##  RUN-SPECIFIC
plotLabel=tumor-HCC1187

### location of scripts for Circos generation and related helper computes
##  USER-SITE-SPECIFIC
circosutils=/data/saskia/CompleteCircosPackage/bin

### location of Complete Genomics Circos plot conf files
##  USER-SITE-SPECIFIC
confDir=/data/saskia/CompleteCircosPackage/circosConf



### path to Circos installation
##  USER-SITE-SPECIFIC
circosbin=/usr/local/circos-0.55/bin
export PATH=${circosbin}:${PATH}


### estimate LAF using a single genome; see caveat regarding single-genome LAF estimates
${circosutils}/addLAFestimates.py \
	-m ${masterVarFile} \
	-c $origCnvDetailsFile \
	-o $cnvDetailsWithLAF

### construct Circos inputs, copy required conf files, run Circos,
### and create html wrapper around resulting png files
${circosutils}/circosPlot.py \
	--junction-file ${highConfJcns} \
	--masterVar-file ${masterVarFile} \
	--circos-output-dir ${outDir} \
	--plot-label ${plotLabel} \
	--sample-type ${sampleType} \
	--cnv-details ${cnvDetailsWithLAF} \
	--cnv-segments ${cnvSegmentsFile} \
	--conf-dir ${confDir} \
	--plot-type ${plotType}
