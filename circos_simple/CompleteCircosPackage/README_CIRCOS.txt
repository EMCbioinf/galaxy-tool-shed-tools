02/29/2012 Scripts to generate Circos Plots similar to those created by the Complete Genomics pipeline (releases 2.0 and above).


Author: Aaron Halpern


Three bash scripts (templates) to generate Circos plots:
	sampleRun.normal - for a single normal genome
	sampleRun.somatic - for a tumor/normal pair to visualize somatic mutations (requires cgatools)
	sampleRun.tumor - for a single tumor genome


Also included in this package:
	circosConf - directory of Circos configuration files
	bin - data processing scripts
	exampleCircosLegend.png - example Circos Legend
	exampleCircos-somatic-HCC2218.png -example Circos Somatic Plot
	


Prerequistes
	1) System running the bash shell (Mac and UNIX/LINUX acceptable)
	2) Python (installed)
	3) Circos-0.52 (Note that these scripts are ONLY COMPATIBLE with Circos version 0.52 which can be downloaded from http://circos.ca/software/download/circos/)
		Please ensure that Circos is correctly configured and installed (including the required supplementary Perl modules)
	4) Complete Genomics genome sequence data.  The directory structure and filenames should be kept as delivered.  LIB and MAP directories are not necessary.
	5) genome CRR file (can be dowloaded from ftp.completegenomics.com/ReferenceFiles/)
	6) cgatools (for somatic Circos plots only)


Instructions
	Select the type of Circos plot that you wish to generate (normal, somatic, or tumor).

	In the appropriate template shell script, lines immediately below the comments "RUN-SPECIFIC" or "USER-SITE-SPECIFIC" need to be modified.                                                        
		- Lines below "RUN-SPECIFIC" must be adjusted appropriately for each run.
		- Lines below "USER-SITE-SPECIFIC" must be adjusted to reflect the user's site (paths to binaries etc) but can be left alone from run to run.       

	Save the changes to the shell script (after changing path information), we will use the sampleRun.normal script as an example here.

	On the command line, type:

		bash-3.2$ sh sampleRun.normal 
	
	to execute the script.  The output files will be written to the output directory location specified in the shell script.

	Alternatively, you can make the script executable and run it directly:
	
		bash-3.2$ chmod +x sampleRun.normal
		bash-3.2$ ./sampleRun.normal
     	
     	