#!/bin/env python
import os,sys
import optparse
from subprocess import call
from CircosInputs import CircosInputs 


def runCircos(workDir,plotLabel):
    os.chdir(workDir)

    try:
        retcode = call('circos -conf main.conf', shell=True)
        if retcode < 0:
            raise Exception("Circos command was terminated by signal " + str(-retcode))
        else:
            if retcode > 0:
                raise Exception("Circos command returned " + str(retcode))

    except OSError, e:
        raise Exception("Execution failed: " + e)

    if os.system('mv %s/circos.png %s/circos-%s.png' % (workDir,workDir,plotLabel) ):
        raise Exception('trouble renaming circos plot')


def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(jcnFile='',masterVar='',lohSource='',
                        cnvDetails='',cnvSegments='',
                        sampleType='',
                        confDir='',workDir='',
                        plotType='',useScore=False)

    parser.add_option('--junction-file', dest='jcnFile',
                      help='junction (or junctiondiff, for somatic) file (high confidence)'),
    parser.add_option('--masterVar-file', dest='masterVar',
                      help='cgatools generatemastervar file'),
    parser.add_option('--calldiff-super-locus', dest='lohSource',
                      help='cgatools calldiff superlocus output file'),
    parser.add_option('--cnv-details', dest='cnvDetails',
                      help='CNV details file including LAF estimates'),
    parser.add_option('--cnv-segments', dest='cnvSegments',
                      help='CNV segments file'),
    parser.add_option('--sample-type', dest='sampleType',
                      help='sample type (male, female, male37, female37'),
    parser.add_option('--conf-dir', dest='confDir',
                      help='directory containing circos conf file templates'),
    parser.add_option('--plot-type',dest='plotType',
                      help='type of circos plot (somatic, normal, unmatched tumor)'),
    parser.add_option('--circos-output-dir', dest='workDir',
                      help='location of circos workfiles and final plot')
    parser.add_option('--plot-label', dest='plotLabel',
                      help='single-word label for plot')
    parser.add_option('--use-score-cutoff',dest='useScore',action='store_true',
                      help='use somatic score cutoff rather than somaticeQuality flag')

    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.jcnFile:
        parser.error('junction-file specification required')

    if not options.masterVar:
        parser.error('masterVar-file specification required')

    if not options.cnvDetails:
        parser.error('cnv-details specification required')

    if not options.cnvSegments:
        parser.error('cnv-segments specification required')

    if not options.sampleType:
        parser.error('sample-type specification required')
    if not options.confDir:
        parser.error('conf-dir specification required')
    if not options.workDir:
        parser.error('circos-output-dir specification required')
    if not options.plotType or options.plotType not in ['somatic','normal','tumor']:
        parser.error('plot-type specification ("tumor", "normal" or "somatic") required')



    ci = CircosInputs()

    if options.plotType == 'somatic':
        ci.createSomaticJunctionInputs(options.jcnFile,options.workDir)
    else:
        ci.createInterChrJunctionInputs(options.jcnFile,options.workDir)

    ci.createSnpDensityInputs(options.masterVar,options.workDir)
    
    if options.plotType == 'somatic':
        if not options.lohSource:
            parser.error('calldiff-super-locus specification required')
        ci.createLOHInputs(options.lohSource,options.workDir)

    ci.createLAFInputs(options.cnvDetails,options.workDir)

    if options.plotType in  ['somatic', 'tumor']:
        ci.createCNVLevelInputs(options.cnvSegments,options.workDir)
    else:
        ci.createCNVPloidyInputs(options.cnvSegments,options.workDir)
        
    if options.plotType == 'somatic':
        ci.createSomaticVarTracks(options.masterVar,options.workDir,options.useScore)

    ci.createCircosConfs(options.confDir,options.workDir,
                         options.sampleType,options.plotType)

    ci.createCircosHtml(options.confDir,options.workDir,options.plotType,options.plotLabel)

    runCircos(options.workDir,options.plotLabel)


if __name__ == '__main__':
    main()
    
