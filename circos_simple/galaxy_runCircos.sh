#!/bin/bash

debug="Y"

control_c()
# run if user hits control-c
{
  exit $?
}

#######################################
#  Location specific settings

#scriptsdir="/data/galaxy-dist/tools/trait/circos"     # location of tool directory
#confdir=${scriptsdir}        						  # loacation of circos conf files (circos_JSV.conf, circos_JS.conf etc)
#hg18bed="${scriptsdir}/hg18Genes"
#hg19bed="${scriptsdir}/hg19Genes"
#######################################


# arguments <junctions file> <SNParray probes> <SNParray snps> <varfile> <WG file> <montage file> <per_chr_file> <hg18|hg19> <customplotfile> <custom_region> <genetrack> <imageformat>
echo "arguments: $@"


if [ $# -ne 14 ]
then
	echo "error, unexpected number of arguments in $0"
	exit
fi

junctions=$1
probes=$2
snps=$3
variations=$4
wg_out=$5
montage_out=$6
per_chr_file=$7
build=$8
customplot_file=$9
custom_region=${10//chr/hs}
show_gene_track=${11}
imageformat=${12}
scriptsdir=${13}
confdir=${14}

hg18bed="${scriptsdir}/hg18Genes"
hg19bed="${scriptsdir}/hg19Genes"

touch snps_circos_original
touch probes_circos_original

if [ $debug == "Y" ]
then
	echo "junctions: $junctions"
	echo "probes: $probes"
	echo "snps: $snps"
	echo "variations: $variations"
	echo "wg_out: $wg_out"
	echo "montage_out: $montage_out"

fi


echo "karyotype = data/karyotype/karyotype.human.hg19.txt" > karyotype.txt
if [ $build == "hg18" ]
then
	echo "karyotype = data/karyotype/karyotype.human.hg18.txt" > karyotype.txt
fi

bn=`basename ${junctions}`

junctions_circos="${bn}_J_circos"
probes_circos="${bn}_probes_circos"
snps_circos="${bn}_snps_circos"
variations_circos="${bn}_variations_circos"
variations_circos_chr="${bn}_variations_circos_chr"
bed_junctions="bedfile_junctions_${bn}"
bed_snps="bedfile_snps_${bn}"
impacted_genes="ImpactedGenes.tsv"
circos_out_wg="${bn}_out_wg"
circos_out_montage="${bn}_out_montage"

touch $bed_junctions
touch $bed_snps
touch $impacted_genes



echo ""
echo "galaxy:runCircos.sh: "
echo "junctions $junctions\n probes: $probes\n snps: $snps\n variations: $variations\n wg_out: $wgout\n montage_out: $montage_out\n per_chr_file: $per_chr_file\n" 
echo ""

## conf file
conffile_JSV=${confdir}/circos_JSV.conf





#####################################
#
#      input file conversions
#
#####################################



echo "show = ${show_gene_track}" > showgenes.txt

if [ $junctions != "None" ]
then	
	echo "junctions file specified"	
	if [[ ! -f $junctions_circos ]]
	then
		${scriptsdir}/junctions2circos.sh $junctions $junctions_circos $bed_junctions
	fi

	if [ ${show_gene_track} == "yes" ]
	then
		
		#cat $bed_junctions $bed_snps > ImpactedGenes.tmp
		mv $bed_junctions ImpactedGenes.tmp

		if [ $build == "hg18" ]
		then
			intersectBed -a ImpactedGenes.tmp -b $hg18bed -wb > ImpactedGenes.tmp2
		else
			intersectBed -a ImpactedGenes.tmp -b $hg19bed -wb > ImpactedGenes.tmp2
		fi

		#generate text-track input file
		awk 'BEGIN{
				FS="\t"
				OFS=" "
			}{
				print $1,$11,$12,$19
			}END{

			}' ImpactedGenes.tmp2 > ImpactedGenes.tmp3

		sed -i 's/chr/hs/g' ImpactedGenes.tmp3
		sort -k4 ImpactedGenes.tmp3 > ImpactedGenes.tmp4
		uniq -u -f 3 ImpactedGenes.tmp4 ImpactedGenes.tsv
		#cat ImpactedGenes.tsv
	fi
else
	junctions_circos="None"
fi

if [ $probes != "None" ]
then
		echo "probes file specified"
	
		${scriptsdir}/SNParray2circos.sh $probes $snps $probes_circos $snps_circos	
		
		cp ${snps_circos} snps_circos_original
		cp ${probes_circos} probes_circos_original
		
		#if too many points, reduce (max 25000)
		numpoints=`wc -l $probes_circos |cut -d" " -f1 `
		echo "numpoints probes.txt: $numpoints"
		while [ $numpoints -gt 25000 ]
		do
		
			
			echo "reducing number of datapoints in probes file ($numpoints)"
			
			float_in=${numpoints}/25000
			ceil_val=${float_in/.*}
    		ceil_val=$[$ceil_val+1]
    		
			awk 'BEGIN{
					FS="\t"
					OFS="\t"	
					xlines="'"$ceil_val"'"
				}{
					if(FNR%xlines==1)
						print $0		#print alternating lines
				
				}END{}' $probes_circos > probes_circos_reduced
				
				rm $probes_circos
				mv probes_circos_reduced $probes_circos
				numpoints=`wc -l $probes_circos |cut -d" " -f1 `
				echo "reduced to ($numpoints)"
		done
else		
	probes_circos="None"	
fi	
	
if [ $snps != "None" ]
then			
		echo "snps file specified"
		
		if [ $probes == "None" ] #otherwise we already did this conversion in previous step
		then
			${scriptsdir}/SNParray2circos.sh $probes $snps $probes_circos $snps_circos	
			cp ${snps_circos} snps_circos_original
			cp ${probes_circos} probes_circos_original
		fi
		
		#if too many points, reduce (max 25000)
		numpoints=`wc -l $snps_circos | cut -d" " -f1 `
		echo "numpoints snps.txt: $numpoints"
		while [ $numpoints -gt 25000 ]
		do
		
			float_in=${numpoints}/25000
			ceil_val=${float_in/.*}
    		ceil_val=$[$ceil_val+1]
			
			echo "reducing number of datapoints in probes file ($numpoints)"
			awk 'BEGIN{
					FS="\t"
					OFS="\t"	
					xlines="'"$ceil_val"'"
				}{
					if(FNR%xlines==1)
						print $0
				
				}END{}' $snps_circos > snps_circos_reduced
				
				rm $snps_circos
				mv snps_circos_reduced $snps_circos
				numpoints=`wc -l $snps_circos |cut -d" " -f1 `
				echo "reduced to ($numpoints)"
		done
else
	snps_circos="None"			
fi


if [ $variations != "None" ]
then
	echo "variations file specified"
	if [[ ! -f $variations_circos  ]]
	then
		${scriptsdir}/variations2circos.sh $variations $variations_circos $variations_circos_chr $bed_snps
		echo "variations_circos"
		#cat $variations_circos
	fi
else
	variations_circos="None"	
fi





#####################################
#
#      run circos
#
#####################################

if [ $junctions == "None" ]
then
	echo "show = no" > show_junctions.txt
else
	echo "plotting junctions, file $junctions :"
	head $junctions_circos
	echo "show = yes" > show_junctions.txt
fi

if [ $probes == "None" ]
then
	echo "show = no" > show_probes.txt
else
	echo "plotting probes, file $probes :"
	head $probes_circos
	echo "show = yes" > show_probes.txt
fi

if [ $snps == "None" ]
then
	echo "show = no" > show_snps.txt
else
	echo "plotting snps, file $snps:"
	head $snps_circos
	echo "show = yes" > show_snps.txt
fi

if [ $variations == "None" ]
then
	echo "show = no" > show_variations.txt
else
	echo "plotting variations, file $variations :"
	head $variations_circos
	echo "and per chr files:"
	head $variations_circos_chr
	echo "show = yes" > show_variations.txt
fi	


	
# junctions & SNParrays & variation
if [[ $junctions != "None" && ( $probes != "None" && $snps != "None" ) && $variations != "None" ]]
then 
	echo "junctions and SNParrays and variations"
	${scriptsdir}/runcircos_JSV.sh ${conffile_JSV} ${junctions_circos} ${probes_circos} ${snps_circos} $wg_out $montage_out $per_chr_file $build ${variations_circos} ${variations_circos_chr} $customplot_file $custom_region $imageformat snps_circos_original probes_circos_original
else
	echo "warning, not enough datasets to make a plot, trying anyway"
	${scriptsdir}/runcircos_JSV.sh ${conffile_JSV} ${junctions_circos} ${probes_circos} ${snps_circos} $wg_out $montage_out $per_chr_file $build ${variations_circos} ${variations_circos_chr} $customplot_file $custom_region $imageformat snps_circos_original probes_circos_original

fi










