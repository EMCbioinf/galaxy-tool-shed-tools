#!/bin/bash

# convert junctions file to input for circos to display links
# usage: junctions2circos.sh <input file> <output file>

# output format: hs1 start end hs2 start end

infile=$1
outfile=$2
bedfile=$3

if [ $# -ne 3 ]
then
	echo "error, unexpected number of arguments in $0"
	exit
fi

### convert junctions file
echo "converting junctions files"

fname=`basename $infile`

echo "infile: $infile, outfile: $outfile, fname: $fname"

# remove header and 'chr'
sed '1,13d' $infile > $fname
sed -i 's/chr//g' $fname

# make output for links in circos
awk 'BEGIN{
		FS="\t";
		OFS=" "
	}{
		if($2 != "M" && $6 != "M")
		print "hs"$2,$3,$3,"hs"$6,$7,$7;	
	}END{}' $fname > $outfile


#make bedfile to find impacted genes
awk 'BEGIN{
		FS="\t";
		OFS="\t"
	}{
		if($2 != "M" && $6 != "M"){
			print "chr"$2,$3,$3;	#left side
			print "chr"$6,$7,$7;    #right side
		}

	}END{}' $fname > $bedfile
