#!/bin/bash

#sort_chromosomal_position.sh $infile $chrcol $startcol $endcol $num_headerlines


cp $1 inputfile.tsv
chrcol=$2
startcol=$3
endcol=$4
num_headerlines=$5
outfile=$6

#remember header 
head -$num_headerlines inputfile.tsv > header.tsv

#remove header
sed -i "1,$num_headerlines d" inputfile.tsv

#sort file
sort -k 1,${chrcol}V -k2,${startcol}n inputfile.tsv > out.tsv

#put header back
cat header.tsv out.tsv > $outfile
